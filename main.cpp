#include <iostream>
#include "src/interestRateCurves.cpp"
#include "src/equityOptionPricing.cpp"
#include "src/BermudanSwaption.cpp"
#include "src/ownEquityOptionPricing.cpp"

void printMessage(const std::string& message){
    std::cout << "----------------------------------" << std::endl;
    std::cout << message << std::endl;
    std::cout << "----------------------------------" << std::endl;
}


int main() {

    printMessage("Execute src/interestRateCurves.cpp ...");
    interestRateCurves();

    printMessage("Execute src/equityOptionPricing.cpp ...");
    equityOptionPricing();

    printMessage("Execute src/BermudanSwaption.cpp ...");
    BermudanSwaption();

    printMessage("Execute src/ownEquityOptionPricing.cpp ...");
    ownEquityOptionPricing();

    return 0;
}
