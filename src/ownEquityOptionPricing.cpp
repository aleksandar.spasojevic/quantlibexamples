#include <iostream>
#include <ql/time/calendars/target.hpp>
#include <ql/instruments/vanillaoption.hpp>
#include <ql/exercise.hpp>
#include <ql/pricingengines/vanilla/analyticeuropeanengine.hpp>
#include <ql/quotes/simplequote.hpp>
#include <ql/termstructures/yield/flatforward.hpp>
#include <ql/termstructures/volatility/equityfx/blackconstantvol.hpp>
#include <ql/time/daycounters/business252.hpp>

using namespace QuantLib;
using namespace std;

int ownEquityOptionPricing() {
    /*
     * Pricing Call Options:
     * Need:
     *  - Underlying
     *  - Volatility
     *  - Dividends
     *  - Interest Rate
     *  - Maturity
     *  - Exercise Type
     */

    // Input Parameters for Option construction

    // Option Type
    Option::Type type(Option::Call);
    // Price of underlying now
    Real underlying = 23.44;
    // Strike of the Option
    Real strike = 25;
    Handle<Quote> underlyingH(ext::shared_ptr<Quote>(new SimpleQuote(underlying)));
    // Dividend yield during Option lifetime
    Real dividendYield = 0.0;
    Handle<Quote> dividendYieldH(ext::shared_ptr<Quote>(new SimpleQuote(dividendYield)));
    // riskFree Rate for discounting
    Real riskFreeRate0 = 0.00602;
    ext::shared_ptr<SimpleQuote> riskFreeRate(new SimpleQuote(riskFreeRate0)) ;
    Handle<Quote> interestRateH(riskFreeRate);

    // flat volatility
    Real volatility = 0.3287;
    Date todaysDate(2, June, 2021);
    Settings::instance().evaluationDate() = todaysDate;
    // Calendar type for holidays
    Calendar calendar = TARGET();
    // Time from which onwards the Option is activated
    Date settlementDate(4, June, 2021);
    // Time at which Option expires
    Date maturity = settlementDate + 87;
    // Day count convention used for discounting
    DayCounter dayCounter = Business252();

    cout << "Option Lifetime in days" << endl;
    cout << daysBetween(settlementDate, maturity) << endl;

    // Need: type, strike
    ext::shared_ptr<StrikedTypePayoff> payoff(new PlainVanillaPayoff(type, strike));
    ext::shared_ptr<Exercise> exercise(new EuropeanExercise(maturity));
    VanillaOption europeanOption(payoff, exercise);

    Handle<YieldTermStructure> flatDividendTS(
            ext::shared_ptr<YieldTermStructure>(
                    new FlatForward(settlementDate, dividendYieldH, dayCounter)));

    //Handle<YieldTermStructure>
    ext::shared_ptr<YieldTermStructure> flatTermStructure(
            ext::shared_ptr<YieldTermStructure>(
                    new FlatForward(settlementDate, dividendYieldH, dayCounter)));

    RelinkableHandle<YieldTermStructure> discountingTermStructure;
    discountingTermStructure.linkTo(flatTermStructure);


/*
    Handle<YieldTermStructure> flatTermStructure(
            ext::shared_ptr<YieldTermStructure>(
                    new FlatForward(settlementDate, dividendYieldH, dayCounter)));
*/


    Handle<BlackVolTermStructure> flatVolTS(
            ext::shared_ptr<BlackVolTermStructure>(
                    new BlackConstantVol(settlementDate,
                                         calendar,
                                         volatility,
                                         dayCounter)));

    ext::shared_ptr<BlackScholesMertonProcess>
            bsmProcess( new BlackScholesMertonProcess(
            underlyingH,
            flatDividendTS,
            discountingTermStructure,
            flatVolTS ));

    europeanOption.setPricingEngine(ext::shared_ptr<PricingEngine>(
            new AnalyticEuropeanEngine(bsmProcess)));

    cout << "Today is the " << todaysDate << endl;
    cout << "Initial Stock Price : " << underlying << endl;
    cout << "Strike Price : " << strike << endl;
    cout << "Flat Volatility : " << volatility * 100 << "%" << endl;
    cout << "Maturity as year fraction : " << dayCounter.yearFraction(settlementDate, maturity) << endl;
    cout << "Flat fisk free rate : " << riskFreeRate0 * 100 << "%" << endl;
    cout << "Dividend yield : " << dividendYield * 100 << "%" << endl;

    cout << "----------------------------------------" << endl;
    cout << "--------Options Prices and Greeks-------" << endl;
    cout << "----------------------------------------" << endl;

    cout << "Options's NPV: " << europeanOption.NPV() << endl;
    cout << "Option's Delta: " << europeanOption.delta() << endl;
    cout << "Option's Gamma: " << europeanOption.gamma() << endl;
    cout << "Option's Vega: " << europeanOption.vega() << endl;

    std::vector<Real> factors = {0.5, 0.75, 1, 1.25, 1.5, 2};
    Real newRiskFreeRate = 0;
    for(auto& v: factors){
        newRiskFreeRate = riskFreeRate0 * v;

        riskFreeRate->setValue(riskFreeRate0 * v);
        discountingTermStructure->update();
        cout << newRiskFreeRate << " " << europeanOption.NPV() << endl;
    }


    return 0;
}

